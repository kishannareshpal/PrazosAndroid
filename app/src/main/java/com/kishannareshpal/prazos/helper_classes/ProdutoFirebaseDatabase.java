package com.kishannareshpal.prazos.helper_classes;

public class ProdutoFirebaseDatabase {

//    public static Date toDate(String value) throws ParseException {
//        DateFormat format = new SimpleDateFormat("d MMMM yyyy 'at' HH:mm'am'", Locale.ENGLISH);
//        return format.parse(value);
//    }

    public String barcode;
    public String data_validade;

    public ProdutoFirebaseDatabase() {
        // Default constructor required for calls to DataSnapshot.getValue(Item.class)
    }

    public ProdutoFirebaseDatabase(String barcode, String data_validade) {
        this.barcode = barcode;
        this.data_validade = data_validade;
    }

}
