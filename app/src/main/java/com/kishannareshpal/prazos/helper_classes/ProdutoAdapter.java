package com.kishannareshpal.prazos.helper_classes;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.card.MaterialCardView;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.kishannareshpal.prazos.R;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * RecyclerView.Adapter
 * RecyclerView.ViewHolder
 *
 */

public class ProdutoAdapter extends RecyclerView.Adapter<ProdutoAdapter.ProdutoViewHolder> implements Filterable{

    private Context ctx;
    private List<Produto> produtoList;
    private List<Produto> produtoListFiltered;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }


    public void setOnClickListener(OnItemClickListener listener){
        mListener = listener;
    }


    public ProdutoAdapter(Context ctx, List<Produto> produtoList) {
        this.ctx = ctx;
        this.produtoList = produtoList;
        this.produtoListFiltered = produtoList;
    }


    @NonNull
    @Override
    public ProdutoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.produto_list, parent, false);
        return new ProdutoViewHolder(view, mListener);
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public void onBindViewHolder(@NonNull ProdutoViewHolder holder, int position) {
        Produto produto = produtoListFiltered.get(position);
        holder.tv_produtoId.setText(String.valueOf(produto.getId()));
        holder.tv_produtoName.setText(produto.getName());
        holder.tv_barcode.setText(produto.getBarcode());
        holder.tv_validade.setText(produto.getData_validade());
        if (produto.getIsPast()) {
            holder.tv_validade.setTextColor(ContextCompat.getColor(ctx, R.color.md_black_1000));
        }
//        holder.cv_produtoCard.setCardBackgroundColor(Color.parseColor(produto.getCardBackgroundColorHEX()));

//        holder.cv_produtoCard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return produtoListFiltered.size();
    }



    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    produtoListFiltered = produtoList;
                } else {
                    List<Produto> filteredList = new ArrayList<>();
                    for (Produto row : produtoList) {
                        // here we are looking for product name or barcode match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getBarcode().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    produtoListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = produtoListFiltered;
                return filterResults;
            }

            @Override
            @SuppressWarnings("unchecked")
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                produtoListFiltered = (ArrayList<Produto>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

//    @Override
//    public int getItemCount() {
//        return produtoList.size();
//    }





    public static class ProdutoViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        public MaterialCardView cv_produtoCard;
        public TextView tv_produtoName, tv_barcode, tv_validade, tv_produtoId;

        // i added OnItemClickListener argument to handle, you know, items click event -.-
        ProdutoViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            // this is general
            cv_produtoCard = itemView.findViewById(R.id.cv_produtoCard);
            tv_produtoName = itemView.findViewById(R.id.tv_produtoName);
            tv_barcode     = itemView.findViewById(R.id.tv_produtoBarcode);
            tv_validade    = itemView.findViewById(R.id.tv_produtoValidade);
            tv_produtoId   = itemView.findViewById(R.id.tv_produtoId);

            itemView.setOnCreateContextMenuListener(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            // this is to prevent errors when clicking a card that has no position bound to it (for example, a card that we deleted seconds ago)
                            listener.onItemClick(position);

                        }

                    }
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(this.getAdapterPosition(), 121, 0, R.string.context_menu_edit);
            contextMenu.add(this.getAdapterPosition(), 122, 1, R.string.context_menu_remove);

//            MenuInflater menuInflater = new MenuInflater(ctx);
//            menuInflater.inflate(R.menu.context_menu, contextMenu);
        }
    }


//    public void removeThisItem(int position){
//        produtoList.remove(position);
//        notifyItemRemoved(position);
//    }

}
