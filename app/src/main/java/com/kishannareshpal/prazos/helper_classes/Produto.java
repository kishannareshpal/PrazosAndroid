package com.kishannareshpal.prazos.helper_classes;

public class Produto {

//    public static Date toDate(String value) throws ParseException {
//        DateFormat format = new SimpleDateFormat("d MMMM yyyy 'at' HH:mm'am'", Locale.ENGLISH);
//        return format.parse(value);
//    }




    private String name, barcode, data_validade;
    private int id;
    private boolean isPast;

    public Produto(String name, String barcode, String data_validade, int id, boolean isPast) {
        this.name = name;
        this.barcode = barcode;
        this.data_validade = data_validade;
        this.id = id;
        this.isPast = isPast;
    }

    public String getName() {
        return name;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getData_validade() {
        return data_validade;
    }

    public int getId() {
        return id;
    }

    public boolean getIsPast() {
        return isPast;
    }
}
