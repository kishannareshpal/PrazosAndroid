package com.kishannareshpal.prazos.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.kishannareshpal.prazos.R;
import com.kishannareshpal.prazos.activities.MainActivity;

import static com.kishannareshpal.prazos.application.App.CHANNEL_1_ID;

public class AlarmNotificationReceiver extends BroadcastReceiver {

    // TODO: 7/4/18
    NotificationManagerCompat notificationManagerCompat;

    @Override
    public void onReceive(Context context, Intent intent) {
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
//        Notification.Builder builder = new Notification.Builder(context);
//
//
//        builder.setAutoCancel(true)
//                .setDefaults(Notification.DEFAULT_ALL)
//                .setWhen(System.currentTimeMillis())
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle("Prazo: Alerta")
//                .setContentText("Shit, this thing got outdated");
//
//        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(1, builder.build());
        // TODO: 7/4/18
        notificationManagerCompat = NotificationManagerCompat.from(context);

//        String productName = intent.getStringExtra("productName");
        int notificationId = intent.getIntExtra("notificationId", 1);
        String notificationContentTitle = intent.getStringExtra("notificationTitle");
        String notificationContentMessage = intent.getStringExtra("notificationMessage");

        Intent intentReaction = new Intent(context, MainActivity.class);
        PendingIntent contentPi = PendingIntent.getActivity(context, notificationId, intentReaction, 0);


        Notification notification = new NotificationCompat.Builder(context, CHANNEL_1_ID)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.ic_stat_name)
                .setColorized(true)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setContentTitle(notificationContentTitle) // [Expirou][Atenção]! - {productName} # Localized
                .setColor(context.getResources().getColor(R.color.md_deep_orange_A400))
                .setContentText(notificationContentMessage) // O prazo do produto '{productName}' [expira hoje][expira em {dias restantes} dias]! # Localized
                .setContentIntent(contentPi)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .build();

        notificationManagerCompat.notify(notificationId, notification);
    }
}
