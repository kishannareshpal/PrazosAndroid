package com.kishannareshpal.prazos.room.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.kishannareshpal.prazos.room.converters.Converters;
import com.kishannareshpal.prazos.room.entities.Item;

@Database(entities = {Item.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class MyDatabase extends RoomDatabase {
    public abstract MyDao myDao();
}
