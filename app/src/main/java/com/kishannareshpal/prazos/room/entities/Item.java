package com.kishannareshpal.prazos.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.kishannareshpal.prazos.room.converters.Converters;

import java.util.Calendar;
import java.util.Date;

@Entity(tableName = "produtosItem")
public class Item {

    @PrimaryKey (autoGenerate = true)
    private int id;
    private String barcode;
    private String nome;
    private Date data_validade;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData_validade() {
        return data_validade;
    }

    public void setData_validade(Date data_validade) {
        this.data_validade = data_validade;
    }
}