package com.kishannareshpal.prazos.room.converters;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

public class Converters {

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

//    public static Calendar stringToCal(String value) {
//        if (value != null){
//            Calendar cal = Calendar.getInstance();
//            SimpleDateFormat sdf = new SimpleDateFormat("d'/'M'/'y", Locale.ENGLISH);
//            try {
//                cal.setTime(sdf.parse(value));// all done
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            return cal;
//
//        } else {
//            return null;
//        }
//    }


    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

//    public static String calToString(Calendar cal) {
//        if (cal != null){
//            return cal.toString();
//
//        } else {
//            return null;
//        }
//    }
}
