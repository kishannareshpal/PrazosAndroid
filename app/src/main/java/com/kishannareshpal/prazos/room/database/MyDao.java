package com.kishannareshpal.prazos.room.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.kishannareshpal.prazos.helper_classes.Produto;
import com.kishannareshpal.prazos.room.entities.Item;

import java.util.Date;
import java.util.List;


@Dao
public interface MyDao {

    @Insert
    long addItem(Item item); // to add produto to the "produtos" entity/table.

    @Update
    void updateItem(Item item); // to update produto on the "produtos" entity/table.

    @Query("DELETE FROM produtosItem WHERE id = :produtoId")
    int deleteItemById(int produtoId); // returns the number of deleted rows as 'Int' or nothing 'void'

    @Query("SELECT * FROM produtosItem ORDER BY data_validade ASC")
    List<Item> getAllItems(); // to get all of the produtos inside the entity/table.

    @Query("SELECT * FROM produtosItem WHERE data_validade > :now ORDER BY data_validade ASC")
    List<Item> getAllNotExpiredItems(Date now); // to get all of the produtos inside the entity/table that are [not yet expired].

    @Query("SELECT * FROM produtosItem WHERE data_validade < :now ORDER BY data_validade ASC")
    List<Item> getAllExpiredItems(Date now); // to get all of the produtos inside the entity/table [that are expired].

    @Query("SELECT * FROM produtosItem WHERE barcode = :barcode LIMIT 1")
    List<Item> getFromBarcode(String barcode); // to get all of the produtos inside the entity/table.

    @Query("SELECT id FROM produtosItem WHERE barcode = :barcode LIMIT 1")
    int getIdFromBarcode(String barcode);

    @Query("DELETE FROM produtosItem WHERE data_validade < :now")
    int clearAllExpired(Date now);

}
