package com.kishannareshpal.prazos.events;

import com.kishannareshpal.prazos.room.entities.Item;


// Single POJO class that wraps the single instance of the 'Produto' class

public class ItemEvent {
    private Item item;

    public ItemEvent(Item item) {
        this.item = item;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
