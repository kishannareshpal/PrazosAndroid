package com.kishannareshpal.prazos.events;

import com.kishannareshpal.prazos.room.entities.Item;

import java.util.List;

// Single Pojo class that wraps a list of the instance of the 'Produto' class

public class ItemsEvent {

    public List<Item> itemList;

    public ItemsEvent(List<Item> itemList) {
        this.itemList = itemList;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }
}
