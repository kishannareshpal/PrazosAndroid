package com.kishannareshpal.prazos.application;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;

import com.franmontiel.localechanger.LocaleChanger;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class App extends Application {
    public static final String CHANNEL_1_ID = "channel1";

    public static final List<Locale> SUPPORTED_LOCALES =
            Arrays.asList(
                    new Locale("en", "US"),
                    new Locale("pt", "PT")
            );

    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannel();

        LocaleChanger.initialize(getApplicationContext(), SUPPORTED_LOCALES);

    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            NotificationChannel channel1 = new NotificationChannel(CHANNEL_1_ID, "Prazo", NotificationManager.IMPORTANCE_DEFAULT);
            channel1.setDescription("[PT]: Alertar quando um produto chega à sua data de expiração.\n[EN]: Notify the user when a product expires.");

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel1);
            }
        }

    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleChanger.onConfigurationChanged();
    }





}
