package com.kishannareshpal.prazos.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.andrognito.flashbar.Flashbar;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.kishannareshpal.prazos.R;
import com.kishannareshpal.prazos.notification.AlarmNotificationReceiver;
import com.kishannareshpal.prazos.room.entities.Item;
import com.mikhaellopez.lazydatepicker.LazyDatePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import mehdi.sakout.fancybuttons.FancyButton;

import static com.kishannareshpal.prazos.activities.MainActivity.myDatabase;

public class AddActivity extends AppCompatActivity {


    public static FragmentManager fragmentManager;
    private FancyButton btn_scanBarcode, btn_save, btn_goback;
    private LazyDatePicker lazyDatePicker;
    public TextView tv_barcodeResult, tv_title, tv_validadeText;
    private EditText et_productname;
    List<Item> itemList;

    // Essential Variables
    String barcode = "", product_name = "", data_validade = "", data_validadeRaw;
    Calendar lazyDateSelected;
    int product_id;

    private static final String DATE_FORMAT = "d/M/yyyy";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);


        // Essential initializations
        fragmentManager = getSupportFragmentManager();
        final Activity activity = this;
        // buttons
        btn_scanBarcode = findViewById(R.id.btn_scanBarcode);
        lazyDatePicker = findViewById(R.id.lazyDatePicker);
        btn_save = findViewById(R.id.btn_save);
        btn_goback = findViewById(R.id.btn_goback);
        // textview
        tv_barcodeResult = findViewById(R.id.tv_barcodeResult);
        tv_title = findViewById(R.id.tv_title);
        tv_validadeText = findViewById(R.id.tv_validadeText);
        // edit text
        et_productname = findViewById(R.id.et_productname);

        final Intent toEditIntent = getIntent();


        if (toEditIntent.hasExtra("EDIT_MODE")){ // if its in edit mode
            product_name = toEditIntent.getStringExtra("PRODUTO_NAME");
            product_id = Integer.valueOf(toEditIntent.getStringExtra("PRODUTO_ID"));
            barcode = toEditIntent.getStringExtra("PRODUTO_BARCODE");
            data_validadeRaw = toEditIntent.getStringExtra("PRODUTO_VALIDADE").split("\\s+")[2]; // save this old date for future backup
            data_validade = data_validadeRaw; // only get the date in format: "d/MM/yyyy" (^up there)
            tv_title.setText(getResources().getString(R.string.title_bar_edit)); // toolbar text, next to the back butotn

            tv_validadeText.setTextSize(14);
            String validadeString = getResources().getString(R.string.product_validade_label_onedit, data_validade);
            SpannableString ss = new SpannableString(validadeString); // so i can edit spans of the validadeString;
            ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(R.color.md_white_1000)); // set text color to white
            BackgroundColorSpan bcs = new BackgroundColorSpan(getResources().getColor(R.color.md_deep_orange_300)); // set background color
            ss.setSpan(fcs, 19, 29, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            ss.setSpan(bcs, 19, 29, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            tv_validadeText.setText(ss);


            et_productname.setText(product_name);
            tv_barcodeResult.setText(barcode);

            btn_save.setBackgroundColor(Color.parseColor("#DD2600"));
            btn_save.setFocusBackgroundColor(Color.parseColor("#FF3D00"));
        }



        // Database Entity/Table
        final Item item = new Item();


        // Button to scan barcode
        btn_scanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(activity);
                intentIntegrator.setBeepEnabled(true);
                intentIntegrator.setOrientationLocked(false);
                intentIntegrator.setPrompt(getResources().getString(R.string.barcode_scanner_hint));
                intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                intentIntegrator.setCameraId(0);
                intentIntegrator.initiateScan();
            }
        });


        tv_barcodeResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(AddActivity.this)
                        .title(R.string.dialog_barcode_label)
                        .iconRes(R.drawable.ic_barcode_24dp)
                        .content(R.string.dialog_barcode_et_label)
                        .inputRangeRes(3, 86, R.color.md_deep_orange_400)
                        .input("...", barcode, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do Nothing.
                            }
                        })
                        .positiveText(R.string.btn_continue)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                String inputText_Barcode = null;
                                if (dialog.getInputEditText() != null) {
                                    inputText_Barcode = dialog.getInputEditText().getText().toString();
                                }
                                tv_barcodeResult.setText(inputText_Barcode);
                                barcode = inputText_Barcode;
                            }
                        })
                        .show();

            }
        });

        // When a correct date is typed inside the date picker
        lazyDatePicker.setOnDatePickListener(new LazyDatePicker.OnDatePickListener() {
            @Override
            public void onDatePick(Date dateSelected) {
                lazyDateSelected = toCalendar(dateSelected);
                int month = lazyDateSelected.get(Calendar.MONTH) + 1;
                data_validade = lazyDateSelected.get(Calendar.DAY_OF_MONTH) + "/" + month + "/" + lazyDateSelected.get(Calendar.YEAR);
            }
        });


        // To cancel adding items activity, and go back to home
        btn_goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });




        // To save the changes
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                product_name = et_productname.getText().toString();

                if (product_name.isEmpty() || barcode.isEmpty() || data_validade.isEmpty()){

                    new MaterialDialog.Builder(AddActivity.this)
                            .title(R.string.dialog_warning_title)
                            .iconRes(R.drawable.ic_round_warning_24dp)
                            .titleColorRes(R.color.md_black_1000)
                            .content(R.string.dialog_warning_content)
                            .positiveText(R.string.dialog_okay_btn)
                            .show();

                } else {

                    if (lazyDatePicker.getDate() == null){ // if the user didn't change the date
                        data_validade = data_validadeRaw;
//                        Date re = LazyDatePicker.stringToDate(data_validade, DATE_FORMAT);
//                        Log.v("REEEE1", re.toString());
//
//                        toCalendar(re).add(Calendar.MONTH, 1); <- doesnt work
//                        lazyDateSelected = toCalendar(re);
                    }

                    // disable the button
                    btn_save.setEnabled(false);

                    item.setNome(product_name);
                    item.setBarcode(barcode);
                    item.setData_validade(string2Date(data_validade));

                    long addedItemId;

                    if (toEditIntent.hasExtra("EDIT_MODE")){
                        item.setId(product_id);
                        myDatabase.myDao().updateItem(item);
                        if (lazyDatePicker.getDate() != null) { // if the user didn't change the date
                            setAlarm(item.getNome(), product_id, lazyDateSelected, calendarDaysBetween(Calendar.getInstance(), lazyDateSelected));
                        }

                    } else {
                        addedItemId = myDatabase.myDao().addItem(item);
                        setAlarm(item.getNome(), addedItemId, lazyDateSelected, calendarDaysBetween(Calendar.getInstance(), lazyDateSelected));
                    }

                    String successContent;
                    long timeToShowSuccessDialog;

                    successContent = getResources().getString(R.string.dialog_success_content);
                    timeToShowSuccessDialog = 1700;


                    if (lazyDatePicker.getDate() != null) { // if the user changed the date
                        if (!lazyDateSelected.after(Calendar.getInstance())) {
                            successContent = getResources().getString(R.string.dialog_success_content_alreadyExpired);
                            timeToShowSuccessDialog = 2700;
                        }
                    }

                    final MaterialDialog sdf = new MaterialDialog.Builder(AddActivity.this)
                            .title(R.string.success)
                            .titleColorRes(R.color.md_green_A700)
                            .iconRes(R.drawable.ic_round_done_outline_24dp)
                            .canceledOnTouchOutside(false)
                            .cancelable(false)
                            .content(successContent)
                            .show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                setResult(RESULT_OK);
                                sdf.dismiss();
                                AddActivity.this.finishAfterTransition();
                            } else {
                                sdf.dismiss();
                                setResult(RESULT_OK);
                                AddActivity.this.finish();
                            }
                        }
                    }, timeToShowSuccessDialog);
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (intentResult != null){
            if (intentResult.getContents() != null){
                // Toast.makeText(this, requestCode, Toast.LENGTH_SHORT).show();
                final String result = intentResult.getContents();

                itemList = myDatabase.myDao().getFromBarcode(result);

                tv_barcodeResult.setText(result);
                barcode = result;

            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    String backContentText;

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra("EDIT_MODE")){
            backContentText = getResources().getString(R.string.dialog_back_confirm_edit);
        } else {
            backContentText = getResources().getString(R.string.dialog_back_confirm);
        }

        if (!(product_name.isEmpty() && data_validade.isEmpty() && barcode.isEmpty())){
            new MaterialDialog.Builder(AddActivity.this)
                    .title(R.string.dialog_back_title)
                    .titleColorRes(R.color.md_deep_orange_A400)
                    .content(backContentText)
                    .negativeText(R.string.dialog_no_btn)
                    .positiveText(R.string.dialog_yes_btn)
                    .positiveColorRes(R.color.md_deep_orange_A400)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            AddActivity.super.onBackPressed();
                        }
                    })
                    .show();
        } else {
            super.onBackPressed();
        }
    }




    /*
    *
    * Helper Methods
    *
    * */

    // Convert to date type
//    public static Date toDate(String value) throws ParseException {
//        DateFormat format = new SimpleDateFormat("d'/'m'/'yyyy", Locale.ENGLISH);
//        return format.parse(value);
//    }

    public static Date string2Date(String value) {
        SimpleDateFormat format = new SimpleDateFormat("d'/'M'/'y", Locale.getDefault());
        try {
            return format.parse(value); // return the Date object'.

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }


    // to show notification on expiree date
    public void setAlarm(String productName, long notificationIdRaw, Calendar when, long daysBefore){
        /* UNCOMMENT FOR TEST ONLY */
            // Calendar c = Calendar.getInstance();
            // c.add(Calendar.SECOND, 30);
            // when = c;
        /* ----------------------- */


        int notificationId = (int) notificationIdRaw;
        int howManyDaysBefore = 0;

        if (when.after(Calendar.getInstance())){
            AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(this, AlarmNotificationReceiver.class);
            intent.putExtra("productName", productName);
            intent.putExtra("notificationId", notificationId);

            // Notify on the Exact Date
            intent.putExtra("notificationTitle", getResources().getString(R.string.notification_exact_title, productName));
            intent.putExtra("notificationMessage", getResources().getString(R.string.notification_exact_content, productName));
            PendingIntent pi_expired = PendingIntent.getBroadcast(this, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            if (alarmManager != null) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, when.getTimeInMillis(), pi_expired);
            }

            /* Comment IF TESTING */
            if (daysBefore > 11){
                when.add(Calendar.DAY_OF_MONTH, -10);
                howManyDaysBefore = 10;

            } else if (daysBefore > 4){
                when.add(Calendar.DAY_OF_MONTH, -3);
                howManyDaysBefore = 3;

            } else if (daysBefore > 2) {
                when.add(Calendar.DAY_OF_MONTH, -1);
                howManyDaysBefore = 1;
            }
            /* ----------------------- */


            /* Uncomment For Test Only */
                // when.add(Calendar.SECOND, -8);
            /* ----------------------- */

            // Notify some days before that it will expire soon:
            intent.putExtra("notificationTitle", getResources().getString(R.string.notification_remind_title, productName));
            intent.putExtra("notificationMessage", getResources().getQuantityString(R.plurals.notification_remind_content, howManyDaysBefore, productName, howManyDaysBefore));


            PendingIntent pi_toBeExpired = PendingIntent.getBroadcast(this, notificationId+1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (alarmManager != null) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, when.getTimeInMillis(), pi_toBeExpired);
            }


        } else {
            Log.v("notifff4", "Already Expired product");
            new Flashbar.Builder(AddActivity.this)
                    .gravity(Flashbar.Gravity.BOTTOM)
                    .duration(Flashbar.DURATION_LONG)
                    .title(R.string.dialog_warning_title)
                    .titleColorRes(R.color.md_yellow_A700)
                    .message(R.string.dialog_warning_already_expired)
                    .backgroundDrawable(R.drawable.flashbar_bg)
                    .build().show();
        }

    }


    // to cancel notification of a certain product expiree date
//    public void cancelAlarm(String productName, long notificationIdRaw){
//        int notificationId = (int) notificationIdRaw;
//        Calendar c = Calendar.getInstance();
//        c.add(Calendar.SECOND, 10);
//
//        if (!c.before(Calendar.getInstance())){
//            AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//            Intent intent = new Intent(this, AlarmNotificationReceiver.class);
//            intent.putExtra("productName", productName);
//            intent.putExtra("notificationId", notificationId);
//
//            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, notificationId, intent, 0);
//            if (alarmManager != null) {
//                alarmManager.cancel(pendingIntent);
//
//            } else {
//                Toast.makeText(this, getResources().getString(R.string.dialog_cancel_notification_error), Toast.LENGTH_SHORT).show();
//            }
//        }
//    }


    /**
     * Compute the number of calendar days between two Calendar objects.
     * The desired value is the number of days of the month between the
     * two Calendars, not the number of milliseconds' worth of days.
     * @param startCal The earlier calendar
     * @param endCal The later calendar
     * @return the number of calendar days of the month between startCal and endCal
     */
    public static long calendarDaysBetween(Calendar startCal, Calendar endCal) {

        // Create copies so we don't update the original calendars.
        Calendar start = Calendar.getInstance();
        start.setTimeZone(startCal.getTimeZone());
        start.setTimeInMillis(startCal.getTimeInMillis());

        Calendar end = Calendar.getInstance();
        end.setTimeZone(endCal.getTimeZone());
        end.setTimeInMillis(endCal.getTimeInMillis());


        // Set the copies to be at midnight, but keep the day information.
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);

        end.set(Calendar.HOUR_OF_DAY, 0);
        end.set(Calendar.MINUTE, 0);
        end.set(Calendar.SECOND, 0);
        end.set(Calendar.MILLISECOND, 0);

        // At this point, each calendar is set to midnight on
        // their respective days. Now use TimeUnit.MILLISECONDS to
        // compute the number of full days between the two of them.

        return TimeUnit.MILLISECONDS.toDays(
                Math.abs(end.getTimeInMillis() - start.getTimeInMillis()));
    }
}
