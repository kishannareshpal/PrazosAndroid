package com.kishannareshpal.prazos.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.card.MaterialCardView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.andrognito.flashbar.Flashbar;
import com.bluehomestudio.progressimage.ProgressPicture;
import com.franmontiel.localechanger.LocaleChanger;
import com.franmontiel.localechanger.utils.ActivityRecreationHelper;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.kishannareshpal.prazos.R;
import com.kishannareshpal.prazos.events.ItemsEvent;
import com.kishannareshpal.prazos.helper_classes.Produto;
import com.kishannareshpal.prazos.helper_classes.ProdutoAdapter;
import com.kishannareshpal.prazos.notification.AlarmNotificationReceiver;
import com.kishannareshpal.prazos.room.database.MyDatabase;
import com.kishannareshpal.prazos.room.entities.Item;
import com.santalu.emptyview.EmptyView;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import mehdi.sakout.fancybuttons.FancyButton;

public class MainActivity extends AppCompatActivity {

    CoordinatorLayout cl_parentLayout;
    MaterialCardView materialCardView;
    FancyButton btn_add, btn_clear;
    RecyclerView rv_produtos;
    List<Item> itemList;
    List<Produto> produtoList;
    ProdutoAdapter produtoAdapter;
    Spinner sp_showItemsAs;
    EmptyView emptyView;
    ProgressBar pb_loading;
    SearchView searchView;
    ProgressPicture iv_logo;


    public static final String MY_PREFS_NAME = "mySharedPref";
    public static int EDIT_REQUEST_CODE = 8;
    public static int ADD_REQUEST_CODE = 9;

    private static String ADMOB_ID = "ca-app-pub-2181029585862550~2511094378";
    private static String INTERSTITIAL_AD_UNIT_ID = "ca-app-pub-2181029585862550/5728284652";
    private static String TEST__INTERSTITIAL_AD_UNIT_ID = "ca-app-pub-3940256099942544/1033173712";
    String version; // app version

    int lastSpinnerPosition = -1; // not selected yet. the initial spinner positioin. This is a K-variable for animation stuff.
    private InterstitialAd mInterstitialAd;
    public static MyDatabase myDatabase;
    private Executor executor = Executors.newSingleThreadExecutor();


    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        boolean isFirstTime = prefs.getBoolean("isFirstTime", true);


        // Init Ads
        MobileAds.initialize(this,ADMOB_ID);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(INTERSTITIAL_AD_UNIT_ID);

        iv_logo = findViewById(R.id.iv_logo);

        materialCardView = findViewById(R.id.mc_materialCard);
        emptyView = findViewById(R.id.empty_view);
        btn_add = findViewById(R.id.btn_add);
        btn_clear = findViewById(R.id.btn_clearList);
        rv_produtos = findViewById(R.id.rv_produtos);
        sp_showItemsAs = findViewById(R.id.sp_showItemsAs);
        cl_parentLayout = findViewById(R.id.parentLayout);
        pb_loading = findViewById(R.id.pb_loading);
        produtoList = new ArrayList<>();


        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = findViewById(R.id.sv);
//        final MaterialSearchView searchView = findViewById(R.id.sv);
        if (searchManager != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setMaxWidth(Integer.MAX_VALUE);

        rv_produtos.setHasFixedSize(false);
        rv_produtos.setLayoutManager(new CustomLinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)); // set the list layout style to vertical.

        myDatabase = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "produtosDB")
                  .allowMainThreadQueries()
                  .build();

        final Date now = new Date();


        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
                    version = pInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }


                new MaterialDialog.Builder(MainActivity.this)
                        .iconRes(R.drawable.ic_logo_round)
                        .title(R.string.dialog_about_title)
                        .titleColorRes(R.color.md_deep_orange_A400)
                        .content(getResources().getString(R.string.dialog_about_content, version))
                        .backgroundColorRes(R.color.md_grey_50)
                        .contentColorRes(R.color.md_grey_900)
                        .neutralText(R.string.dialog_close_btn).neutralColorRes(R.color.md_grey_900)
                        .positiveText(R.string.change_lang).positiveColorRes(R.color.md_deep_orange_A700)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                new MaterialDialog.Builder(MainActivity.this)
                                        .iconRes(R.drawable.ic_twotone_language_24dp)
                                        .title(R.string.change_lang)
                                        .alwaysCallSingleChoiceCallback()
                                        .items(R.array.languages)
                                        .itemsCallbackSingleChoice(getLocaleForLanguageChooser(), new MaterialDialog.ListCallbackSingleChoice() {
                                            @Override
                                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                                switch (which){
                                                    case 0: // English
                                                        LocaleChanger.setLocale(new Locale("en", "US"));
                                                        ActivityRecreationHelper.recreate(MainActivity.this, true);
                                                        break;

                                                    case 1: // Portuguese
                                                        LocaleChanger.setLocale(new Locale("pt", "PT"));
                                                        ActivityRecreationHelper.recreate(MainActivity.this, true);
                                                        break;
                                                }
                                                return true;
                                            }
                                        })
                                        .negativeText(R.string.dialog_cancel_btn)
                                        .show();
                            }
                        })
                        .show();

                loadTheAnnoyingAd();
            }
        });


        // If it's the first time the user opens the app, show the Welcome TapTap Target
        if (isFirstTime) {
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putBoolean("isFirstTime", false);
            editor.apply();

            TapTargetView.showFor(this,                 // `this` is an Activity
                    TapTarget.forView(findViewById(R.id.iv_logo), getResources().getString(R.string.welcome_target_title), getResources().getString(R.string.welcome_target_description))
                            // All options below are optional
                            .outerCircleColor(R.color.md_deep_orange_A700)      // Specify a color for the outer circle
                            .outerCircleAlpha(0.9f)            // Specify the alpha amount for the outer circle
                            .targetCircleColor(R.color.white)   // Specify a color for the target circle
                            .titleTextSize(20)                  // Specify the size (in sp) of the title text
                            .titleTextColor(R.color.white)      // Specify the color of the title text
                            .descriptionTextSize(14)            // Specify the size (in sp) of the description text
                            .descriptionTextColor(R.color.md_grey_100)  // Specify the color of the description text
                            .textColor(R.color.md_white_1000)            // Specify a color for both the title and description text
                            .dimColor(R.color.md_black_1000)            // If set, will dim behind the view with 30% opacity of the given color
                            .drawShadow(true)                   // Whether to draw a drop shadow or not
                            .cancelable(true)                  // Whether tapping outside the outer circle dismisses the view
                            .tintTarget(true)                   // Whether to tint the target view's color
                            .transparentTarget(false)           // Specify whether the target is transparent (displays the content underneath)
                            .targetRadius(25),                  // Specify the target radius (in dp)
                    new TapTargetView.Listener() {      // The listener can listen for regular clicks, long clicks or cancels
                        @Override
                        public void onTargetClick(TapTargetView view) {
                            super.onTargetClick(view);      // This call is optional
                            iv_logo.performClick();
                        }
                    }
            );
        }


        executor.execute(new Runnable() {
            @Override
            public void run() {
                sp_showItemsAs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        switch (i){
                            case 0: // Not expired items only
                                iv_logo.startAnimation();
                                pb_loading.setVisibility(View.VISIBLE);
                                btn_clear.setVisibility(View.GONE);

                                if (lastSpinnerPosition == 1){ // if the click comes from EXPIRED position
                                    btn_add.setTranslationX(btn_clear.getX()-btn_add.getX());
                                    btn_add.setVisibility(View.VISIBLE);
                                    btn_add.animate().translationX(0).setInterpolator(new DecelerateInterpolator()).withEndAction(new Runnable() {
                                        @Override
                                        public void run() {
                                            lastSpinnerPosition = 0;
                                        }
                                    }).start();

                                } else {
                                    btn_add.setVisibility(View.VISIBLE);
                                    lastSpinnerPosition = 0;
                                }

                                itemList = myDatabase.myDao().getAllNotExpiredItems(now);
                                EventBus.getDefault().post(new ItemsEvent(itemList));
                                searchView.setIconified(true);
                                break;




                            case 1: // Expired items only
                                iv_logo.startAnimation();
                                pb_loading.setVisibility(View.VISIBLE);
                                btn_add.setVisibility(View.GONE);
                                loadTheAnnoyingAd();

                                if (lastSpinnerPosition == 0 || lastSpinnerPosition == 2){ // if the click comes from EXPIRED position
                                    btn_clear.setTranslationX(btn_add.getX()-btn_clear.getX());
                                    btn_clear.setVisibility(View.VISIBLE);
                                    btn_clear.animate().translationX(0).setInterpolator(new DecelerateInterpolator()).withEndAction(new Runnable() {
                                        @Override
                                        public void run() {
                                            lastSpinnerPosition = 1;
                                        }
                                    }).start();

                                } else {
                                    btn_clear.setVisibility(View.VISIBLE);
                                    lastSpinnerPosition = 1;
                                }

                                itemList = myDatabase.myDao().getAllExpiredItems(now);
                                searchView.setIconified(true);
                                EventBus.getDefault().post(new ItemsEvent(itemList));
                                break;




                            case 2: // All of the items. Expired or not.
                                iv_logo.startAnimation();
                                pb_loading.setVisibility(View.VISIBLE);
                                btn_clear.setVisibility(View.GONE);
                                loadTheAnnoyingAd();

                                if (lastSpinnerPosition == 1){ // if the click comes from EXPIRED position
                                    btn_add.setTranslationX(btn_clear.getX()-btn_add.getX());
                                    btn_add.setVisibility(View.VISIBLE);
                                    btn_add.animate().translationX(0).setInterpolator(new DecelerateInterpolator()).withEndAction(new Runnable() {
                                        @Override
                                        public void run() {
                                            lastSpinnerPosition = 2;
                                        }
                                    }).start();

                                } else {
                                    btn_add.setVisibility(View.VISIBLE);
                                    lastSpinnerPosition = 2;
                                }

                                itemList = myDatabase.myDao().getAllItems();
                                searchView.setIconified(true);
                                EventBus.getDefault().post(new ItemsEvent(itemList));
                                break;
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        });



        // basically populate the recycler view via the worker's thread; (or background thread or whatever room database needs)
        EventBus.getDefault().register(this);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
              @Override
              public boolean onQueryTextSubmit(String query) {
                  // filter recycler view when query submitted
                  try{
                      produtoAdapter.getFilter().filter(query);
                  } catch (Exception e) {
                      Log.v("Search Filter – K: ", e.getMessage());
                  }
                  return false;
              }

              @Override
              public boolean onQueryTextChange(String query) {
                  // filter recycler view when text is changed
                  try{
                      produtoAdapter.getFilter().filter(query);
                  } catch (Exception e) {
                      Log.v("Search Filter – K: ", e.getMessage());
                  }
                  return false;
              }
        });


        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);

                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(MainActivity.this,
                                btn_add,
                                Objects.requireNonNull(ViewCompat.getTransitionName(btn_add)));

                startActivityForResult(intent, ADD_REQUEST_CODE, options.toBundle());

            }
        });


        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(MainActivity.this)
                        .iconRes(R.drawable.ic_delete_sweep_24dp)
                        .title(R.string.dialog_clear_title)
                        .titleColorRes(R.color.md_red_A700)
                        .positiveText(R.string.dialog_clear_positivebtn)
                        .positiveColorRes(R.color.md_red_A700)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                if (myDatabase.myDao().clearAllExpired(new Date()) > 0){
                                    new Flashbar.Builder(MainActivity.this)
                                            .gravity(Flashbar.Gravity.BOTTOM)
                                            .duration(Flashbar.DURATION_LONG)
                                            .title(R.string.success)
                                            .titleColorRes(R.color.md_green_A700)
                                            .message(R.string.dialog_clear_confirmation)
                                            .backgroundDrawable(R.drawable.flashbar_bg)
                                            .build().show();

                                    pb_loading.setVisibility(View.VISIBLE);
                                    iv_logo.startAnimation();
                                    itemList = myDatabase.myDao().getAllNotExpiredItems(now);

                                    sp_showItemsAs.setSelection(0);

//                                    btn_clear.setVisibility(View.GONE);
                                    itemList = myDatabase.myDao().getAllNotExpiredItems(now);
                                    EventBus.getDefault().post(new ItemsEvent(itemList));

                                } else {
                                    new Flashbar.Builder(MainActivity.this)
                                            .gravity(Flashbar.Gravity.BOTTOM)
                                            .duration(Flashbar.DURATION_LONG)
                                            .title(R.string.fail)
                                            .titleColorRes(R.color.md_red_A700)
                                            .message(R.string.dialog_clear_error)
                                            .backgroundDrawable(R.drawable.flashbar_bg)
                                            .build().show();

                                }

                            }
                        })
                        .negativeText(R.string.dialog_cancel_btn)
                        .show();
            }
        });
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void itemsEventHandler(ItemsEvent event) {
        itemList = event.getItemList();
        produtoList.clear();
        Date now = new Date();
        Date validadeFromDB;


        if (!itemList.isEmpty()){
            emptyView.content().show();

            for (Item item : itemList){
                validadeFromDB = item.getData_validade();

                produtoList.add(new Produto(item.getNome(), item.getBarcode(), getResources().getString(R.string.valido_ate_text) + " " + dateToFormatedString(validadeFromDB), item.getId(), validadeFromDB.before(now)));
//                for (int i = 0; i<20; i++){
//                  validadeFromDB = item.getData_validade();
//                  produtoList.add(new Produto("Product " + i, item.getBarcode(), "Válido até: " + "10.05.2019", item.getId() + i, validadeFromDB.before(now)));
//                }
            }


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pb_loading.animate().translationY(-7).setInterpolator(new LinearInterpolator()).setDuration(100).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            pb_loading.setVisibility(View.GONE);
                            iv_logo.stopAnimation();
                            pb_loading.animate().translationY(0);
                        }
                    }).start();
                }
            }, 1000);


        } else {
            btn_clear.setVisibility(View.GONE);


            switch (sp_showItemsAs.getSelectedItemPosition()){
                case 0:
                    emptyView.empty().setEmptyTitle(getResources().getString(R.string.emptyview_nonexpired_title));
                    emptyView.empty().setEmptyText(getResources().getString(R.string.emptyview_nonexpired));
                    break;

                case 1:
                    emptyView.empty().setEmptyTitle(getResources().getString(R.string.emptyview_expired_title));
                    emptyView.empty().setEmptyText(getResources().getString(R.string.emptyview_expired));
                    break;

                case 2:
                    emptyView.empty().setEmptyTitle(getResources().getString(R.string.emptyview_all_title));
                    emptyView.empty().setEmptyText(getResources().getString(R.string.emptyview_all));
                    break;
            }

            emptyView.empty().show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pb_loading.animate().translationY(-7).setInterpolator(new LinearInterpolator()).setDuration(100).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            pb_loading.setVisibility(View.GONE);
                            iv_logo.stopAnimation();
                            pb_loading.animate().translationY(0);
                        }
                    }).start();
                }
            }, 1000);

        }


        produtoAdapter = new ProdutoAdapter(MainActivity.this, produtoList);
        rv_produtos.setAdapter(produtoAdapter);


        produtoAdapter.setOnClickListener(new ProdutoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                View view = Objects.requireNonNull(rv_produtos.findViewHolderForAdapterPosition(position)).itemView;
                Intent intent = new Intent(MainActivity.this, AddActivity.class);

                intent.putExtra("EDIT_MODE"       , true);
                intent.putExtra("PRODUTO_NAME"    , getSelectedProdutInfo(position, R.id.tv_produtoName    ));
                intent.putExtra("PRODUTO_ID"      , getSelectedProdutInfo(position, R.id.tv_produtoId      ));
                intent.putExtra("PRODUTO_BARCODE" , getSelectedProdutInfo(position, R.id.tv_produtoBarcode ));
                intent.putExtra("PRODUTO_VALIDADE", getSelectedProdutInfo(position, R.id.tv_produtoValidade));


                ActivityOptionsCompat options = ActivityOptionsCompat.makeClipRevealAnimation(view, (int) view.getX(), (int) view.getY(), view.getWidth(), view.getHeight());
                startActivityForResult(intent, EDIT_REQUEST_CODE, options.toBundle());


            }
        });

    }




    @Override
    public boolean onContextItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case 121:
                String pn = getSelectedProdutInfo(menuItem.getGroupId(), R.id.tv_produtoName);
                String bc = getSelectedProdutInfo(menuItem.getGroupId(), R.id.tv_produtoBarcode);
                String id = getSelectedProdutInfo(menuItem.getGroupId(), R.id.tv_produtoId);
                String vd = getSelectedProdutInfo(menuItem.getGroupId(), R.id.tv_produtoValidade);
                openEditActivity(id, pn, bc, vd);
                break;

            case 122:

                final int delete_productId = Integer.valueOf(getSelectedProdutInfo(menuItem.getGroupId(), R.id.tv_produtoId));
                final String delete_productName = getSelectedProdutInfo(menuItem.getGroupId(), R.id.tv_produtoName);

                new MaterialDialog.Builder(MainActivity.this)
                        .iconRes(R.drawable.ic_twotone_delete_forever_24dp)
                        .title(getResources().getString(R.string.dialog_remove_title, delete_productName))
                        .titleColorRes(R.color.md_red_A700)
                        .positiveText(R.string.dialog_remove_positivebtn)
                        .positiveColorRes(R.color.md_red_A700)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                int i = myDatabase.myDao().deleteItemById(delete_productId); // returns the number of affected rows

                                dialog.dismiss();

                                if (i > 0){
                                    new Flashbar.Builder(MainActivity.this)
                                            .gravity(Flashbar.Gravity.BOTTOM)
                                            .duration(Flashbar.DURATION_LONG)
                                            .title(getResources().getString(R.string.dialog_remove_confirmation, delete_productName))
                                            .titleColorRes(R.color.md_white_1000)
                                            .backgroundDrawable(R.drawable.flashbar_bg)
                                            .build().show();

                                } else {
                                    new Flashbar.Builder(MainActivity.this)
                                            .gravity(Flashbar.Gravity.BOTTOM)
                                            .duration(Flashbar.DURATION_LONG)
                                            .title(R.string.fail)
                                            .message(getResources().getString(R.string.dialog_remove_error, delete_productName))
                                            .titleColorRes(R.color.md_red_A700)
                                            .backgroundDrawable(R.drawable.flashbar_bg)
                                            .build().show();
                                }

                                produtoList.remove(menuItem.getGroupId());
                                produtoAdapter.notifyItemRemoved(menuItem.getGroupId()); // this is to update the recycler view.

                                cancelAlarm(delete_productName, delete_productId);

                            }
                        })
                        .negativeText("Cancelar")
                        .show();
//
                break;
        }

        return super.onContextItemSelected(menuItem);

    }





    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        if (!searchView.isIconified()){
            searchView.setIconified(true);

        } else {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getResources().getString(R.string.exit_confirmation), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);

        }
    }


    /**
     * –––
     * – Helper Functions
     * –––
     **/

    // get the item's information. context menu stuff
    public String getSelectedProdutInfo(int position, int viewRes){
        TextView tv = Objects.requireNonNull(rv_produtos.findViewHolderForAdapterPosition(position)).itemView.findViewById(viewRes);
        return tv.getText().toString();
    }




    private void openEditActivity(String id, String produtoName, String barcode, String validade){

        Intent intent = new Intent(MainActivity.this, AddActivity.class);

        intent.putExtra("EDIT_MODE", true);
        intent.putExtra("PRODUTO_NAME", produtoName);
        intent.putExtra("PRODUTO_ID", id);
        intent.putExtra("PRODUTO_BARCODE", barcode);
        intent.putExtra("PRODUTO_VALIDADE", validade);

        startActivityForResult(intent, EDIT_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        final Date now = new Date();

        if ((requestCode == EDIT_REQUEST_CODE) || (requestCode == ADD_REQUEST_CODE)){
            if (resultCode == RESULT_OK){
                // some changes where made;
                iv_logo.startAnimation();
                pb_loading.setVisibility(View.VISIBLE);

                switch (sp_showItemsAs.getSelectedItemPosition()){
                    case 0:
                        itemList = myDatabase.myDao().getAllNotExpiredItems(now);
                        break;

                    case 1:
                        loadTheAnnoyingAd();
                        itemList = myDatabase.myDao().getAllExpiredItems(now);
                        break;

                    case 2:
                        loadTheAnnoyingAd();
                        itemList = myDatabase.myDao().getAllItems();
                        break;

                    default:
                        sp_showItemsAs.setSelection(0);
                        itemList = myDatabase.myDao().getAllNotExpiredItems(now);
                        break;
                }

                EventBus.getDefault().post(new ItemsEvent(itemList));

            }
        }

    }



    // get the locale as an int for language chooser default option
    public int getLocaleForLanguageChooser(){
        switch (LocaleChanger.getLocale().toString()){
            case "en_US":
                return 0;

            case "pt_PT":
                return 1;

            default:
                return -1;
        }
    }


    // load interstitial (full screen) ad:
    public void loadTheAnnoyingAd(){
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("ADMOB", "The interstitial wasn't loaded yet.");
        }
    }


    // Convert to date type
    public static String dateToFormatedString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("d'/'M'/'y", Locale.ENGLISH);
        return sdf.format(date);
    }

    // Layout manager for recycler view
    public class CustomLinearLayoutManager extends LinearLayoutManager {
        public CustomLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
        }
    }


    // to cancel notification of a certain product expiree date
    public void cancelAlarm(String productName, long notificationIdRaw){
        int notificationId = (int) notificationIdRaw;

        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmNotificationReceiver.class);
        intent.putExtra("productName", productName);
        intent.putExtra("notificationId", notificationId);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, notificationId, intent, 0);
        PendingIntent pendingIntent2 = PendingIntent.getBroadcast(this, notificationId+1, intent, 0);

        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
            alarmManager.cancel(pendingIntent2);

        } else {
            new Flashbar.Builder(MainActivity.this)
                    .gravity(Flashbar.Gravity.BOTTOM)
                    .duration(Flashbar.DURATION_LONG)
                    .title(R.string.fail)
                    .message(R.string.dialog_cancel_notification_error)
                    .titleColorRes(R.color.md_red_A700)
                    .backgroundDrawable(R.drawable.flashbar_bg)
                    .build().show();
        }
    }
}
